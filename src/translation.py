from src.constants import GENETIC_CODE


def translate(first, second, third):
    try:
        codon = first + second + third
        return GENETIC_CODE[codon]
    except KeyError:
        return None
