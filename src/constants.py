PHE = 'F'
LEU = 'L'
ILE = 'I'
MET = 'M'
VAL = 'V'
SER = 'S'
PRO = 'P'
THR = 'T'
ALA = 'A'
TYR = 'T'
STOP = '-'
HIS = 'H'
GLN = 'Q'
ASN = 'N'
LYS = 'K'
ASP = 'D'
GLU = 'E'
CYS = 'C'
TRP = 'W'
ARG = 'R'
GLY = 'G'

GENETIC_CODE = {
  'UUU': PHE,
  'UUC': PHE,
  'UUA': LEU,
  'CUU': LEU,
  'CUC': LEU,
  'CUA': LEU,
  'CUG': LEU,
  'AUU': ILE,
  'AUC': ILE,
  'AUA': ILE,
  'AUG': MET,
  'GUU': VAL,
  'GUC': VAL,
  'GUA': VAL,
  'GUG': VAL,
  'UCU': SER,
  'UCC': SER,
  'UCA': SER,
  'UCG': SER,
  'CCU': PRO,
  'CCC': PRO,
  'CCA': PRO,
  'CCG': PRO,
  'ACU': THR,
  'ACC': THR,
  'ACA': THR,
  'ACG': THR,
  'GCU': ALA,
  'GCC': ALA,
  'GCA': ALA,
  'GCG': ALA,
  'UAU': TYR,
  'UAC': TYR,
  'UAA': STOP,
  'UAG': STOP,
  'CAU': HIS,
  'CAC': HIS,
  'CAA': GLN,
  'CAG': GLN,
  'AAU': ASN,
  'AAC': ASN,
  'AAA': LYS,
  'AAG': LYS,
  'GAU': ASP,
  'GAC': ASP,
  'GAA': GLU,
  'GAG': GLU,
  'UGU': CYS,
  'UGC': CYS,
  'UGA': STOP,
  'UGG': TRP,
  'CGU': ARG,
  'CGC': ARG,
  'CGA': ARG,
  'CGG': ARG,
  'AGU': SER,
  'AGC': SER,
  'AGA': ARG,
  'AGG': ARG,
  'GGU': GLY,
  'GGC': GLY,
  'GGA': GLY,
  'GGG': GLY
}
