import unittest
from src.constants import GENETIC_CODE


class TestConstants(unittest.TestCase):

    def test_dictionary(self):
        self.assertEqual('M', GENETIC_CODE['AUG'], "Should be M")

    def test_fail(self):
        with self.assertRaises(KeyError):
            GENETIC_CODE['XYZ']
