import unittest
from src.translation import translate


class TestTranslation(unittest.TestCase):

    def test_codon(self):
        aa = translate('A', 'U', 'G')
        self.assertEqual(aa, 'M', "Should be M")

    def test_not_valid_codon(self):
        aa = translate('R', 'X', 'V')
        self.assertIsNone(aa, "Should be none")
