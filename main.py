#!/usr/bin/python

import sys
from src.translation import translate

nucleotides = sys.argv[1:]
aminoacid = translate(*nucleotides)

print(aminoacid)
